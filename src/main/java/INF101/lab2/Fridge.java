package INF101.lab2;


import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    ArrayList<FridgeItem> itemList= new ArrayList<>(20);
    int Size = 20;

    public int nItemsInFridge(){
        return itemList.size();
    }
    
    public int totalSize(){
        return  Size;
        }
    
    public boolean placeIn(FridgeItem item){
        if (itemList.size() < Size){
            itemList.add(item);
            return true;
        }
        else{return false;}
    }
    public void takeOut(FridgeItem item){
        if (itemList.contains(item)){
            itemList.remove(item);}
            
        else{ throw new NoSuchElementException();}}

    
    public void emptyFridge() {
        itemList.removeAll(itemList);
    }

    
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiList = new ArrayList<>();
        for (FridgeItem item : itemList) {
        if (item.hasExpired()){
            expiList.add(item);} 
        }
        itemList.removeAll(expiList);
        return expiList;
    }
    }
    

